# Каталог сервисов HR

[[_TOC_]]

---

# Getting started

- Ответственный за направление: 

## Портфель проектов

- [ ] HR-платформа
- [ ] 1С ЗУП
- [ ] ATS (платформа для подбора персонала)
- [ ] LMS (платформа обучения и развития)



```plantuml
!include <C4/C4>
!include <C4/C4_Context>

!include https://gitlab.com/soprun/arch/-/raw/main/services/05_domen_hr/sys-xxxxx_1C_ZUP/1C_ZUP.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/05_domen_hr/sys-xxxxx_ATS/ATS.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/05_domen_hr/sys-xxxxx_HR_platform/hr_platform.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/05_domen_hr/sys-xxxxx_LMS/LMS.puml
'!include https://gitlab.com/soprun/arch/-/raw/main/services/07_domen_obespech_func/sys-xxxxx_1C_DO
'!include https://gitlab.com/soprun/arch/-/raw/main/services/07_domen_obespech_func/sys-xxxxx_EDO

```