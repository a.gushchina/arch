# Портал (сайт strana.com)

```plantuml
@startuml Портал (сайт strana.com)

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/portal/portal.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/portal/portal-container.puml

@endum
```