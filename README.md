[[_TOC_]]

---

# Getting started

## Что такое запись архитектурного решения?

**Запись архитектурного решения (ADR)** — это документ, который фиксирует важное архитектурное решение, принятое вместе с
его контекстом и последствиями.

**Архитектурное решение (AD)** — это выбор дизайна программного обеспечения, который отвечает важным требованиям.

**Журнал решений по архитектуре (ADL)** — это коллекция всех ADR, созданных и поддерживаемых для конкретного проекта (или
организации).

**Архитектурно -значимое требование (ASR)** — это требование, которое оказывает измеримое влияние на архитектуру программной
системы.

Все это находится в рамках темы управления знаниями об архитектуре (AKM).

Цель этого документа — предоставить краткий обзор ADR, как их создавать и где искать дополнительную информацию.

Сокращения:
- **AD**: архитектурное решение
- **ADL**: журнал решений по архитектуре
- **ADR**: запись решения по архитектуре
- **AKM**: управление знаниями об архитектуре
- **ASR**: архитектурно-значимое требование


Подробнее: https://github.com/joelparkerhenderson/architecture-decision-record

---

Основные задачи подготовки проекта:

- [ ] Перенести все модели в единый реестр
- [ ] Проработать "Основные положения" по разработке проектов
- [ ] Проработать ASR
- [ ] Проработать шаблоны
- [ ] Автоматизировать развертывание CI&CD
- [ ] Автоматизировать развертывание для других сотрудников через статический сайт
- [ ] Зашита и принятие проекта как основу архитектуры в компании
- [ ] Провести обучение и аттестацию ключевых участников

### [Основные положения](docs/decisions/основные-положения.md)

### Источники

- [ ] https://app.diagrams.net/#G1Qglc46G9NlP2OQKqVeBQzL6NVXQSUGyU
- [ ] https://app.diagrams.net/#G1vrQstPndD-ONeyzJwYElKeoRCmbikdIP
- [ ] https://miro.com/app/board/uXjVOnx4AU8=/
- [ ] https://lucid.app/lucidchart/c7513caf-7916-40ca-aac9-64e58ea5bdf6/edit?invitationId=inv_bc3742c0-9e01-4ae8-a54e-a7da9fca8825&page=Y2XN-zhQtKov#
- [ ] [Глобальный план переезда на микросервисы](https://docs.google.com/document/d/18p3dN-ylwF15-csuiYaWlHv8xQCijx_Dt10BY6Aj6N4/edit)

# Архитектура сервисов Страна Девелопмент

 {+ Все завершенные проекты должны быть отмечены галочкой после завершения работ. +}

## [Портфель проектов клиентского сервиса](services/04_domen_customer_services)

- [ ] [Личный кабинет брокера](services/04_domen_customer_services/LK-broker)
- [ ] [Личный кабинет клиента](services/04_domen_customer_services/LK-client)
- [ ] [Личный кабинет панель менеджера](services/04_domen_customer_services/LK-manager-panel)
- [ ] [Портал (сайт strana.com)](services/04_domen_customer_services/portal)
- [ ] [Динамическое ценообразование (ДЦО)](services/04_domen_customer_services/DCO)

## Портфель проектов HR

- [ ] Платформа для подбора персонала (ATS)
- [ ] Платформа для ведения кадрового делопроизводства (1С ЗУП)
- [ ] Платформа для реализации кадровых БП (HR-платформа)
- [ ] Платформа для обучения и развития (LMS)

## Портфель проектов финансы

- [ ] 

## Портфель проектов девелопмент

- [ ] 

## Портфель проектов строительство

- [ ] 

## Портфель проектов ~~обеспечение функции~~

- [ ] ??? (1С ДО)
- [ ] ??? (EDO)
- [ ] База знаний


```plantuml
@startuml Архитектура сервисов Страна Девелопмент

!include <C4/C4>
!include <C4/C4_Context>

!include https://gitlab.com/soprun/arch/-/raw/main/containers/actors/agent.puml
!include https://gitlab.com/soprun/arch/-/raw/main/containers/actors/customer.puml
!include https://gitlab.com/soprun/arch/-/raw/main/containers/actors/employeeManager.puml

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/context.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/MIS/MIS.puml

Rel(agent, LKBroker, "")
Rel(customer, portal, "")
Rel(customer, LKClient, "")
Rel(employeeManager, ManagerPanel, "")

@endum
```
